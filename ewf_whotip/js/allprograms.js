//-----------------------------------------------------------------------------
// public functions
// ------------------------------------------------------------------------------

// This is a hack for the old primary-allprograms panel design.  Since we don't have MVC in the design here we simulate a very basic MVC model.
// MODEL 		- The data passed in is a JSON object that contains all data this page needs.
// VIEW 		- We Ajax load allprograms.html and allprograms.css into DOM.
// CONTROLLER	- We then hijack keystroks so we can handle it first.  For those keys we ignore we let them fall back to original handler.
// 
// CSS engineer can take these 3 files (html+js+css) and start tweaking stylesheets without knowing anything about js logic.
// JS engineer can test this page alone without backend and any other javascript logic.  One page at a time, no worries about messing other pages.
function allprograms (wrapper, classStr, oncreate, ondestroy, horizontalMenu, backAtBottom, noAjax) {
    this.wrapper = wrapper;
    this.classStr = classStr;
    this.oncreate = oncreate;
    this.ondestroy = ondestroy;
    this.horizontalMenu = horizontalMenu;
    this.noAjax = noAjax;
    this.backAtBottom = backAtBottom;
    this.data = null;
    this.subdata = null;
    this.idx = 3;
	this.subidx = 0;
	this.firstitem = 0;
}

allprograms.prototype.render = function(data) {
	this.data = data;
	if(!this.noAjax) { // Ajax loading
    	// load allprograms.css
		$("<link/>", {rel: "stylesheet", type: "text/css", href: "css/allprograms.css"}).appendTo("head");
		
		// load allprograms.html
		var container = $('#' + this.wrapper);

		if(container.length<=0) {
			$('body').prepend('<div id="' + this.wrapper + '"></div>');
		}
		
		var context = this;
		
		$('#' + this.wrapper).load("allprograms.html #allprograms", function(){
            // callback to outside so they can do stuff (such as hiding div)
          if(context.oncreate)
               context.oncreate();
		
		context._renderData();
		});
    }
    else {
    	this._renderData();
    }
}

allprograms.prototype.destroy = function() {
	// restore key handler
	this._restoreKeypressed();
	
	//remove HTML from DOM and remove CSS file link	
	$('#' + this.wrapper).remove();
	$('link[href="css/allprograms.css"]').remove();
	
	// callback tooutside so they can bring up other UI
	if(this.ondestroy)
		this.ondestroy();
};

// ------------------------------------------------------------------------------
// internal functions
// ------------------------------------------------------------------------------
// All the keyboard event that we will process
allprograms.prototype._navigate = function(key)	{
	var curr = $("#allprograms #selections a.active");
	var currbutton = $('div#allprograms div#buttons a.active');
	var currsubmenu = $('div#allprograms div#selection div#subselections a.active');
	
	if (curr.length>=0 && (key=='ENTER' || key=="RIGHT") && (currbutton.attr("id")!='mainmenu' && currbutton.attr("id")!='more')) {
		this._click(curr,key);
		return true;
	}

	if (curr.length>=0 && (key=='ENTER') && (currbutton.attr("id")!='mainmenu' || currbutton.attr("id")!='more')) {
		this._click(curr,key);
		return true;
	}
	
	if (curr.length>=0 && key=="LEFT" && currbutton.attr("id")!='mainmenu') {
		this._click(curr,key);
		return true;
	}
	
	if ( key=='MENU' || key =='HOME' || key=='POWR' || key=='END') {
		this.destroy();
		return false;
	}
		
		if(currsubmenu.length>=1) {
			this._changeSubDataFocus(key);
		}
		else if(curr.length>=1||currbutton.length>=1) { 		
			this._changeDataFocus(key);
		} 
		return true;
	
	
msg('allprograms.js - returning key to main' + key);
return false;
};
// When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
allprograms.prototype._click = function(jqobj,key) {

	var curr = $('div#allprograms div#selections a.active');
	var currbutton = $('div#allprograms div#buttons a.active');
	var currsubmenu = $('div#allprograms div#selection div#subselections a.active');
	var last = $('div#allprograms div#selections a:last-child');
	var first = $('div#allprograms div#selections a:first-child');	
	var data = this.data;
	var next = curr;

	if (currbutton.attr("id")=='mainmenu' && key=='ENTER') {
		this.destroy();
	}

	else if (currbutton.attr("id")=='more' && key=='ENTER') {
		var firstitem = this.firstitem;
		firstitem = firstitem + 7;
		
		if(firstitem>=data.allprograms.length)
			firstitem = 0;
		this.setselections(firstitem);
		$("#allprograms #buttons a.active").removeAttr("class");
		var newsel = $("#allprograms #selections a.active");
		this._focus(newsel);					
    	return true; 
	}
	
	else if(currsubmenu.length<=0 && (currbutton.attr("id")=='back' || key=='LEFT')) { // back button
		this.destroy();
	} 
	else if(currsubmenu.length>=1 && key=='LEFT') {
		currsubmenu.removeClass('active');
		curr.addClass('active');
	} 
	else if(currsubmenu.length>=1) {
		var newpage = new watchvideo('watchvideo-wrapper', 'allprogramsvideo'
			, function(){
				$('#allprograms-wrapper').hide();
			}, function(){
				$('#allprograms-wrapper').show();
				allprograms.prototype.setpage();
			},0,1);
		var id = currsubmenu.attr("id");

		var options = allprogramsSELECTION(id);
		newpage.render(options);		
	
		return true;
	} 
	else {
		
			var firstsubselection = $('div#allprograms div#selection div#subselections a:first-child');
			msg('moving to subsel');
			$('div#allprograms div#selection div#subselections').scrollTop(0);  		
	
			this._focus(firstsubselection);
		
	}
	
	return;
};

// When an object is focused by using keyboard, or mouse hover
allprograms.prototype._focus = function(jqobj) {
	
	jqobj.addClass('active');
	if(jqobj.hasClass('menu')) {	// left menu - folder
		this.idx = jqobj.index();

		var backidx = this.backAtBottom? this.subdata.length : 0;
		if(this.idx < 0 || this.idx > this.subdata.length) {
			return;
		}
		else if(this.idx == backidx) { // back button
			$('#allprograms #background').removeClass('textoverlay');
		}
		else {
			$('#allprograms #background').addClass('textoverlay');
			this._renderSubData(jqobj);
		}
	}
	else if(jqobj.hasClass('submenu')) {	// right menu - video
		this.subidx = jqobj.index();
	}
	return;
}

// When an object lose focus or mouse leave
allprograms.prototype._blur = function(jqobj) {
	if(jqobj) {
		jqobj.removeClass('active');
	}
	else {

		//$('div#allprograms div#selections a.active').removeClass('active');
	}
	this.idx = -1;
	$('#allprograms #selection #body').html('');
	
	
	return;
}

allprograms.prototype._renderData = function() {
    $('#' + this.wrapper + " #allprograms").addClass(this.classStr);
    var data = this.data;
    
	// show the div
	$('#' + this.wrapper + " #allprograms").show();

	$('<p></p>').prependTo('div#allprograms div#label').html("Education<br/>Health Video Library");
	var menulabel = $('#navigation #menutabs #menutab.active').text();
	
	var selections = $('div#allprograms div#selections');
			
	this.subdata = data.allprograms;
	var sub = this.data.allprograms;			
	
	var context = this
	this.setselections(0);
	var firstobj = null;
	// dynamically render data if neccesary                                    
	// wire up the mouse event
	$('div#allprograms div#selections a').click(function(){
		context._blur();
		context._focus($(this));
		context._click($(this));
		return false;
	});
			

			// now we take over the key handler from navigation.js
	this._overwriteKeypressed();
};

allprograms.prototype._renderSubData = function(jqobj) {
	msg('moving to top  1');
	$('div#allprograms div#selection div#subselections').scrollTop(0);  		
	
	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	var startpos = this.firstitem;
	dataidx = dataidx + startpos;
	if(typeof this.subdata[dataidx] != 'undefined') {
		var sub = this.subdata[dataidx];	
	} else {
		var sub = this.subdata;
	}
	this._renderPage(sub);	
}

allprograms.prototype._renderPage = function(jqobj) {
	var data = allprogramsPANEL(jqobj.tag);
	var subdata = data[jqobj.tag];
	var selection = $('div#allprograms div#selection div#subselections');
	selection.empty();
	for(var key in subdata) {
		var Title = subdata[key].label;
		var id = subdata[key].hUId;
		var localEntryUID = subdata[key].tag;
		var o = $('<a href="#" class="asset ellipsis"></a>').appendTo(selection).html(Title).addClass('submenu ' + key).attr("id",localEntryUID);
        
        var isBookmarked = (subdata[key].ticket || subdata[key].bookmark == 'Y');
		if(isBookmarked)
			o.addClass('selected');
	}
        
    selection.show();
       
}

// Hijack the global keypressed() function
allprograms.prototype._overwriteKeypressed = function() {
	this.original_keypressed = window.keypressed;
	var context = this;
	window.keypressed = function(keyCode) {
		context._keypressed(keyCode);
	}
};

// Restore the key handler
allprograms.prototype._restoreKeypressed = function() {
	
	window.keypressed = this.original_keypressed;
};

allprograms.prototype._keypressed = function(keyCode) {
	var akey = getkeys(keyCode);
	
	if(window['msg'])	
		msg('(allprograms.js?) keypress: code = ' + keyCode + ' - ' + akey  );
	// remember to let it fall back to original handler if we ignore it
	if(!this._navigate(akey)) {
		if(this.original_keypressed)
			return this.original_keypressed(keyCode);
	}
	
	return true;
};

// Focus control
allprograms.prototype._changeDataFocus = function(key) {
msg('changedatafocus');
	var curr = $('div#allprograms div#selections a.active');
	if (curr.length<=0) 
		curr = $('div#allprograms div#buttons a.active');
	var currbutton = $('div#allprograms div#buttons a.active');
	var next = '';
	var nextsel = '';
	
	switch(key) {
		case 'LEFT':
		case 'UP':
			var nextsel = curr.attr("prev");
			break;
		case 'DOWN':
			var nextsel = curr.attr("next");
			break;	
		case 'RIGHT':
				if(currbutton.attr("id")=='more' ||	currbutton.attr('id')=='mainmenu') {					
					var nextsel = curr.attr("next");					
				} else {
					msg('moving to subsel');
					next = $('div#allprograms div#selection div#subselections a:first-child');					
					$('div#allprograms div#selection div#subselections').scrollTop(0);  		
					if(next.length<=0)
						next.addClass('active');
					return;
				}				
				
	}
	
	if(nextsel=='more' || nextsel =='mainmenu')  {
		next = $('div#allprograms div#buttons a#'+nextsel);
		var selections = $("#allprograms #selection #subselections");	
		selections.empty();
		}
	else 
		next = $('div#allprograms div#selections a#'+nextsel);
	
	this._blur(curr);
	this._focus(next);
};


// Focus control
allprograms.prototype._changeSubDataFocus = function(key) {
	msg('changesubdatafocus');
	var currsub = $('div#allprograms div#selection div#subselections a.active');
	var nextsub = currsub;
	var last = $('div#allprograms div#selection div#subselections a:last-child');
	var first = $('div#allprograms div#selection div#subselections a:first-child');
	var firstpos = $('a').index(first);
	var currpos = $('a').index(currsub);
	var lastpos = $('a').index(last);
	var totalheight = parseInt(lastpos/7)*294;
	var selsub = $('div#allprograms div#selection div#subselections');
	var idx = currsub.index();
				var pos = $('div#allprograms div#selection div#subselections').scrollTop();  
				var height = $('div#allprograms div#selection div#subselections').height();  

	
	if(currsub.length <= 0) {
		return;
	}
		
	switch(key) {
		case 'UP':
				pos = pos - height;
				nextsub = currsub.prev();
				if(nextsub.length<=0)
					nextsub = $('div#allprograms div#selection div#subselections a:last-child');
				break;
		case 'DOWN':				
			pos = pos + height;
			nextsub = currsub.next();
			if(nextsub.length<=0) {
				nextsub = $('div#allprograms div#selection div#subselections a:first-child');
				pos = 0-height;
			}
			break;
		case 'ENTER':			
					var dataidx = this.backAtBottom? this.idx : this.idx-1;
msg('1');
					if(typeof this.subdata[dataidx] != 'undefined') {
						var sub = this.subdata[dataidx];	
						var newpage = new watchvideo('watchvideo-wrapper', 'allprogramsvideo'
							, function(){
								$('#allprograms').hide();
							}, function(){
								$('#allprograms').show();
								allprograms.prototype.setpage();
							},0,1);							
							var id = curr.attr("id");
							var options = allprogramsSELECTION(id);
							newpage.render(options);
							return;
						}					
			break;
	}
	
	if(nextsub != currsub) {

		currsub.removeClass('active');
		nextsub.addClass('active');
		var currsub = $('div#allprograms div#selection div#subselections a.active');

		var page = parseInt(idx/7);
		page = page + 1;
		var idx = currsub.index();
		
		if(idx%7==6&&idx!=0&&key=='UP') 			
			$('div#allprograms div#selection div#subselections').scrollTop((page-2)*294);  				
		
		if(idx%7==0&&idx!=0&&key=='DOWN') 
			$('div#allprograms div#selection div#subselections').scrollTop(page*294);  		
		
		if(currpos==lastpos&&key=='DOWN')
			$('div#allprograms div#selection div#subselections').scrollTop(pos);  		
		if(currpos==firstpos && key == 'UP') 
			$('div#allprograms div#selection div#subselections').scrollTop(totalheight);  		
	}	
			

			
}

allprograms.prototype.setpage = function() {
	
var currsub = $('div#allprograms div#selection div#subselections a.active');
var idx = currsub.index();
var page = parseInt(idx/7);

$('div#allprograms div#selection div#subselections').scrollTop(page*294);  		

msg('insetpage' + idx + ' ' + page);
return;
}

allprograms.prototype.setselections = function(startpos) {
	msg('setselections');
	var data = this.data;
	
	var selections = $("#allprograms #selections");	
	selections.empty();
	var max = 6;
	if(startpos+max>data.allprograms.length-1) {
		len = data.allprograms.length-1;
	} else {
		len = startpos+max;
	}
	
	for(var i = startpos;i<=len;i++){
		var Title = data.allprograms[i].label;
		
		var previous=data.allprograms[i==0?data.allprograms.length-1:i-1];
		var current=data.allprograms[i];
		var next=data.allprograms[i==data.allprograms.length-1?0:i+1];
		
		var localEntryUID = current.tag;
		var prevUID = previous.tag;
		var nextUID = next.tag;
		
		if(startpos == i) {
			prevUID = 'mainmenu';
			$('div#allprograms div#buttons a#mainmenu').attr('next',localEntryUID);
		}
		if(i == len) {
			nextUID = 'more';
			$('div#allprograms div#buttons a#more').attr('prev',localEntryUID);
		}

		var o = $('<a href="#"></a>').appendTo(selections).html(Title).addClass('menu ' + i).attr({
																									id: localEntryUID,
																									prev: prevUID,
  																									next: nextUID
																									});
		if(i==startpos) {
			firstobj = o;
			firstobjpos = i;
		}
		
	}
	if (firstobj == null) 
		firstobj = o;
	this._focus(firstobj);
	this.firstitem = firstobjpos;
	
};
	
