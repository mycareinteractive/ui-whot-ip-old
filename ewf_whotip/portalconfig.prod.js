window.portalConfig = {
    "host":	"/",
    "method":"stbservlet",
    "menuhost":"http://192.168.100.10:9080/ams/aceso/getMenuXml?portal=whot",
    "configselection":"http://192.168.100.10:9080/ams/aceso/getConfigSelections",
    "addconfigs":  "http://192.168.100.10:9080/ams/aceso/addConfigSelections",
    "meals": "http://192.168.100.10:9080/ams/aceso/getAgendaJson?days=7&portal=srh&startdate=",
    "proxy":"Proxy/ServerProxy",
    "applicationUID":"60010001",
    "applicationUIDRecover":"60010000",
    "nodeGroup":"1",
    "VODRootHUID":"WHON3",
    "regionChannelGroup":"1",
    "movies":"WHON10",
    "allprograms":"WHON11",								
    "misc": "WHON31",
    "mischomeID" : "10000001",
    "welcomeVideo":"1000000208",
    "RTSPTransport_Enseo":"MP2T/AVP/UDP",
    "RTSPTransport_TCM":"MP2T/DVBC/QAM",
    "ServiceGroup_ENSEO":"2",
    "ServiceGroup_TCM":"1",	
    "ServiceGroup":"2",
    "Poster": "",
    "epgFilePath": "../WEPG/FULLEPG/",
    "EPGChannelType": "0", 	
    "EPGChannelFrequency": "375000", 
    "EPGChannelProgramNumber": "3", 
    "EPGChannelQAMMode": "Auto",
    "EPGProgramSelectionMode": "PATProgram",
    "scenictvChannel":"90",
    "menuChannel":"104",
    "startChannel":"60000040",
    "recoveryPOUID": "9",
    "moviesPOUID": "3",
    "ewflocation":"/index.html", 
    "blackbox":"http://192.168.100.10",
    "VODServerType":"SeaChange",
    "killTreeTime":"120",
    "languageMap": {'es':1, 'vi':2},
    "analyticsProvider": "google",
    "analyticsUrl": "http://192.168.100.10/piwik/",
    "analyticsSite": "UA-43547321-4",
};
